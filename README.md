# Cluster Image

Generate colour palettes from images in R. 

## Running the code
At the moment the code is made up of two functions that need to be run in order. 
First, read the image with `read_image_2dframe`, then generate the colour
palette based on k-means clustering with `cluster_image_rgb`. From there you
can use the `write_gimp_palette.R` function to write your newly generated 
palette to disk for use with GIMP and Inkscape.
