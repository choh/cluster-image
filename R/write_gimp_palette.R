write_gimp_palette <- function(.data, name = "Cluster Palette",
                               overwrite = TRUE) {
#' Write a palette to disk to use with GIMP, Inkscape and probably more.
#' 
#' @param .data The data to work on.
#' @param name The name of the palette.
#' @param overwrite Bool whether to overwrite existing files. Default TRUE. 
  # scale r, g, b values to 255
  scaled_rgb <- round(.data[, 1:3] * 255, 0)
  # need to use lower case for hex colours here 
  scaled_rgb$rgb <- tolower(.data$rgb)
  
  file_name <- tolower(name)
  file_name <- gsub(" ", "_", file_name)
  file_name <- paste0(file_name, ".gpl")
  
  if (!file.exists(file_name) | overwrite) {
    fh <- file(file_name, "w")
    writeLines("GIMP Palette", con = fh)
    writeLines(paste("Name:", name), con = fh)
    apply(scaled_rgb, 1, function(x) {
      writeLines(paste(x, collapse = " "), con = fh)
    })
    close(fh)
  } else {
    stop("File already exists.")
  }
}
